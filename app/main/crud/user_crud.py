#   encoding: utf-8

#   Import Modules
import uuid
import math
from sqlalchemy import or_
from sqlalchemy.orm import Session
from typing import Any, Dict, Optional, Union

from app.main import schemas
from app.main.models import User, Status
from app.main.crud.base import CRUDBase
from app.main.schemas.base import DataList
from app.main.schemas import UserCreate, UserUpdate
from app.main.core.security import get_password_hash, verify_password

#   Class Definitions
class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):

    def add_default_user(self) -> Any:
        pass

    def find_users(
        self, db:Session, status: str,page: int = 0, per_page: int = 10, keyword: str = '', order: str = 'desc',
    ) -> DataList:
        record_query = db.query(User).filter(User.status != Status.DELETED)

        if status:
            record_query = record_query.filter(User.status == status)
        
        if keyword:
            record_query = record_query.filter(or_(
                User.email.ilike(f'%{keyword}%'),
                User.lastname.ilike(f'%{keyword}%'),
                User.firstname.ilike(f'%{keyword}%'),
            ))

        '''
        if order in ['asc', 'ASC']:
            record_query = record_query.order_by(getattr(User, 'created_at').asc())

        if order in ['desc', 'DESC']:
            record_query = record_query.order_by(getattr(User, 'created_at').desc())
        '''

        total = record_query.count()
        result = record_query.offset((page - 1) * per_page).limit(per_page).all()

        return schemas.DataList(
            total= total,
            page = math.ceil(total / per_page),
            current_page = page,
            per_page = per_page,
            data = result
        )

    def find_user(self, db: Session, user_uuid: str) -> Optional[User]:
        return db.query(User).filter(User.uuid == user_uuid).first()
    
    def find_user_by_email(self, db: Session, email: str) -> Optional[User]:
        return db.query(User).filter(User.email == email).first()

    def is_active(self, user: User) -> bool:
        return user.status == Status.ACTIVED

    def create_user(self, db: Session, obj_in: UserCreate) -> User:
        db_obj = User(
            uuid = str(uuid.uuid4()),
            lastname = obj_in.lastname,
            firstname = obj_in.firstname,
            email = obj_in.email,
            password = get_password_hash(obj_in.password)
        )

        db.add(db_obj)

        db.commit()
        db.refresh(db_obj)
        return db_obj

    def update_user(self, db: Session, db_obj: User, obj_in: Union[UserUpdate, Dict[str, Any]]) -> Any:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        if "password" in update_data and update_data["password"]:
            password_hash = get_password_hash(update_data["password"])
            del update_data["password"]
            update_data["password_hash"] = password_hash
        
        return super().update(db, db_obj=db_obj, obj_in=update_data)

    def delete_user(self, db: Session, user_uuid: str) -> str:
        user = db.query(User).filter(User.uuid == user_uuid).first()

        user.status = 'DELETED'

        db.commit()
        db.refresh()
        return 'User successful deleted'

    def authenticate(self, db: Session, email: str, password: str) -> Optional[User]:
        user = self.find_user_by_email(db = db, email = email)
        
        if not user:
            return None
        
        if not verify_password(password, user.password or ''):
            return None
        
        return user

user = CRUDUser(User)