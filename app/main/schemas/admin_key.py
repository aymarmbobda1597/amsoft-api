#   encoding: utf-8

#   Import Modules
from pydantic import BaseModel

#   Class Definitions
class AdminKey(BaseModel):
    key: str