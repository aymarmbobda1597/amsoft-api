#   encoding: utf-8

#   Import Modules
from pydantic import BaseModel
from typing import List, Optional

#   Class Definition
class UserBase(BaseModel):
    email: Optional[str] = None
    lastname: Optional[str] = None
    firstname: Optional[str] = None
    gender: Optional[str] = None
    status: Optional[str] = None

class UserCreate(UserBase):
    pass

class UserUpdate(UserBase):
    uuid: Optional[str] = None

class UserInDBBase(UserBase):
    class Config:
        orm_mode = True

class UserDisplay(UserBase):
    pass

class UserDisplayList(UserBase):
    data: Optional[List[UserDisplay]] = []