#   encoding: utf-8

#   Import Modules
from pydantic import BaseModel

#   Class Definitions
class Msg(BaseModel):
    message: str

class Token(BaseModel):
    token: str