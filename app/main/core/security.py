#   encoding: utf-8

#   Import Modules
import jwt

from typing import Any, Optional, Union
from datetime import datetime, timedelta
from passlib.context import CryptContext

from .config import Config

#   Variables Defintions
pwd_context = CryptContext(schemes = ['bcrypt'], deprecated = 'auto')

#   Functions Definitions
def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)

def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)

def generate_access_token(subject: Union[str, Any], expires_delta: timedelta = None) -> Any:
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(
            minutes=Config.ACCESS_TOKEN_EXPIRE_MINUTES
        )
    payload = {
        "exp": expire,
        "sub": str(subject)
    }

    return jwt.encode(payload, Config.SECRET_KEY, algorithm = Config.ALGORITHM)

def generate_password_reset_token(email: str) -> str:
    delta = timedelta(hours=Config.EMAIL_RESET_TOKEN_EXPIRE_HOURS)
    now = datetime.utcnow()
    expires = now + delta
    exp = expires.timestamp()

    return jwt.encode(
        {
            "exp": exp,
            "nbf": now,
            "sub": email
        }, 
        Config.SECRET_KEY, algorithm = Config.ALGORITHM,
    )

def verify_password_reset_token(token: str) -> Optional[str]:
    try:
        decoded_token = jwt.decode(token, Config.SECRET_KEY, algorithms = [Config.ALGORITHM])
        
        return decoded_token["email"]
    except jwt.InvalidTokenError:
        return None