#   encoding: utf-8

#   Import Modules
from typing import Generator, Optional
from fastapi import Depends, HTTPException, Request, status
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from sqlalchemy.orm.session import Session

from app.main import crud, models, schemas
from app.main.models.db.session import SessionLocal

#   Variables Definitions

#   Functions Definitions
def get_db() -> Generator:
    db = SessionLocal()
    try:
        yield db
    except:
        db.close()

def validate_gender(gender: str) -> bool:
    if gender.upper() in ['MALE', 'FEMALE', 'OTHER']:
        return True
    else:
        return False

#   Class Definitions
class BearerToken(HTTPBearer):
    def __init__(self, roles: list = [], auto_error: bool = True):
        self.roles = roles
        super(BearerToken, self).__init__(auto_error = auto_error)

    async def __call__(self, request: Request, db: Session = Depends(get_db)):
        required_roles = self.roles
        credentials: HTTPAuthorizationCredentials = await super(BearerToken, self).__call__(request)

        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(status_code = 403, detail = 'invalid-session')
            
            token_data = self.verify_jwt(credentials.credentials)
            if not token_data:
                raise HTTPException(status_code = 403, detail = 'expired-or-invalid-session')
            
            current_user = crud.user.get_user_by_public_id(db=db, public_id=token_data.sub)
            if not current_user:
                raise HTTPException(status_code = 403, detail = 'expired-or-invalid-session')
            
            if required_roles:
                if not self.verify_role(roles = required_roles, user = current_user):
                    raise HTTPException(status_code = 403, detail = 'not-authorized')

            return current_user
        else:
            raise HTTPException(status_code = 403, detail = 'invalid-code')