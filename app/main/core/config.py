#   encoding: utf-8

#   Import Modules
import os

from pydantic import BaseSettings

#   Functions Definitions
def get_secret(secret_name, default):
    try:
        with open('/run/secret/{0}'.format(secret_name), 'r') as secret_file:
            return secret_file.read().strip()
    except IOError:
        return os.getenv(secret_name, default)

#   Class Definitions
class ConfigClass(BaseSettings):
    ALGORITHM: str = get_secret('ALGORITHM', 'HS256')
    SECRET_KEY: str = get_secret("SECRET_KEY", 'secret_key')
    ADMIN_KEY: str = get_secret('ADMIN_KEY', 'AMSOFTKey2022')

    API_V1_STR: str = get_secret('EMAIL_FROM_NAME', '/api/v1')
    PROJECT_VERSION: str = get_secret('PROJECT_VERSION', '0.1')
    PROJECT_NAME: str = get_secret('PROJECT_NAME', 'AMSOFT API')

    SQLALCHEMY_DATABASE_URL: str = 'sqlite:///./databases/amsoft.db'
    #SQLALCHEMY_DATABASE_URL: str = 'postgresql://username:password@postgresserver/amsoft'

    EMAIL_RESET_TOKEN_EXPIRE_HOURS: int = int(get_secret("EMAIL_RESET_TOKEN_EXPIRE_HOURS", 48))
    # 60 minutes * 24 hours * 365 days = 365 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = int(get_secret("ACCESS_TOKEN_EXPIRE_MINUTES", 60 * 24 * 365))
    
Config = ConfigClass()