#   encoding: utf-8

#   Import Modules
from datetime import datetime
from enum import Enum
from sqlalchemy.sql import func
from dataclasses import dataclass
from sqlalchemy.orm import relationship
from sqlalchemy import Column, DateTime, ForeignKey, String, Table, types

from app.main.models.db.base_class import Base

#   Enumerations Definitions
class Status(str, Enum):
    UNVALIDATED = "UNVALIDATED"
    ACTIVED = "ACTIVED"
    UNACTIVED = "UNACTIVED"
    DELETED = "DELETED"

class Gender(str, Enum):
    MALE = "MALE"
    FEMININE = "FEMININE"
    OTHER = "OTHER"

#   Models Definitions
user_group = Table('user_group', Base.metadata,
    Column('user_uuid', String, ForeignKey('users.uuid', ondelete="CASCADE")),
    Column('group_uuid', String, ForeignKey('groups.uuid', ondelete="CASCADE"))
)

role_group = Table('role_group', Base.metadata,
    Column('group_uuid', String, ForeignKey('groups.uuid', ondelete="CASCADE")),
    Column('role_uuid', String, ForeignKey('roles.uuid', ondelete="CASCADE"))
)

@dataclass
class Role(Base):

    """ User Role Model for storing user role related details """

    __tablename__ = "roles"
    
    uuid: str = Column(String, primary_key = True, unique = True, nullable = False)

    title: str = Column(String, index = True, unique= True, nullable= False)

    groups: any = relationship("UserGroup",secondary = role_group, back_populates = "users_roles")

    date_added: any = Column(DateTime(timezone=True), server_default=func.now())
    date_modified: any = Column(DateTime(timezone=True), server_default=func.now(), onupdate=datetime.now())

    def __repr__(self):
        return '<Role: id: {} title_fr: {} />'.format(self.uuid, self.title)

@dataclass
class UserGroup(Base):

    """ User Group Model for storing user group related details """

    __tablename__ = "groups"
    
    uuid: str = Column(String, primary_key = True, unique = True, nullable = False)

    title: str = Column(String, index = True, unique= True, nullable= False)

    # Users
    users_group: any = relationship("User",secondary = user_group, back_populates = "groups")

    # Roles
    users_roles: any = relationship("UserRole",secondary = role_group, back_populates = "groups")

    date_added: any = Column(DateTime(timezone=True), server_default=func.now())
    date_modified: any = Column(DateTime(timezone=True), server_default=func.now(), onupdate=datetime.now())

    def __repr__(self):
        return '<UserGroup: id: {} title_fr: {} />'.format(self.uuid, self.title)

@dataclass
class User(Base):
    __tablename__ = 'users'

    uuid: str = Column(String, primary_key = True, unique = True, nullable = False)

    email: str = Column(String, index = True, unique= True, nullable= False)
    password: str = Column(String, index = True, nullable= False, default = '')

    lastname: str = Column(String, index = True, nullable = True, default = '')
    firstname: str = Column(String, index = True, nullable = True, default = '')

    gender: str = Column(types.Enum(Gender), index=True, nullable=True, default=Gender.OTHER)
    status: str = Column(types.Enum(Status), index=True, nullable=True, default=Status.UNACTIVED)

    avatar_uuid = Column(String, ForeignKey('documents.uuid', ondelete = "CASCADE"), nullable = True)
    avatar = relationship("Document", foreign_keys = [avatar_uuid])

    groups: any = relationship("UserGroup",secondary = user_group, back_populates = "users_group")

    last_connexion_date: any = Column(String(255), default=None)
    date_added: any = Column(DateTime(timezone=True), server_default=func.now())
    date_modified: any = Column(DateTime(timezone=True), server_default=func.now(), onupdate=datetime.now())

    def __repr__(self) -> str:
        return "<User 'firstname: {self.firstname}' 'lastname: {self.lastname}'>"