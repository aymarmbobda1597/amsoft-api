#   encoding: utf-8

#   Import Modules
from dataclasses import dataclass
from sqlalchemy import Column, String, Integer

from app.main.models.db.base_class import Base

#   Models Definitions
@dataclass
class Document(Base):
    __tablename__ = 'documents'

    uuid: str = Column(String, primary_key = True, unique = True, nullable = False)

    filename: str = Column(String, index = True, unique = True, nullable = False)

    url: str = Column(String, index = True, nullable = True, default = '')
    mimetype: str = Column(String, index = True, nullable = True, default = '')

    width = Column(Integer, default = 0, nullable = True)
    height = Column(Integer, default = 0, nullable = True)
    size = Column(Integer, default = 0, nullable = True)

    def __repr__(self) -> str:
        return "<Document 'uuid: {self.uuid}' 'filename: {self.filename}'>"