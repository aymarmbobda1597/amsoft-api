#   encoding: utf-8

#   Import Modules
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.main.core.config import Config

engine = create_engine(Config.SQLALCHEMY_DATABASE_URL, connect_args={ "check_same_thread": False }) # Only for SQLite
#engine = create_engine(Config.SQLALCHEMY_DATABASE_URL, pool_pre_ping=True, pool_size=32, max_overflow=64)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)