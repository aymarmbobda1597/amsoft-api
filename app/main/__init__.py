#   encoding: utf-8

#   Import Modules
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.main.core.config import Config
from .controllers.router import api_router

#   Variables Definitions
description = ''' 
    This is AMSOFT API. All request in regards to the application can be found here,
    in general most of the methods require you being as an active user to be able to access
'''

app = FastAPI(
    title= Config.PROJECT_NAME,
    openapi_url= f'{Config.API_V1_STR}/openapi.json',
    description= description,
    version= Config.PROJECT_VERSION
)

#   Middlewares Definitions
app.include_router(api_router, prefix= Config.API_V1_STR)
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["*"]
)