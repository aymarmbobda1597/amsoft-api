#   encoding: utf-8

#   Import Modules
from fastapi import APIRouter

from .user_controller import router as user
from .migration_controller import router as migration
from .administration_controller import router as administration

#   Variables Definitions
api_router = APIRouter()

#   Functions Definitions
api_router.include_router(administration)
api_router.include_router(migration)
api_router.include_router(user)