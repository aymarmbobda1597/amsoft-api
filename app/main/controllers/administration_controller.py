#   encoding: utf-8

#   Import Modules
from typing import Any
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends

from app.main import models, schemas
from app.main.core import dependencies

#   Variables Definitions
router = APIRouter(
    prefix= '/administration',
    tags= ['administration']
)

#   Functions Definitions
@router.get('/users')
def listing_of_users(
    page: int = 0,
    per_page: int = 10,
    keyword: str = '',
    order: str = 'desc',
    db: Session = Depends(dependencies.get_db)
) -> Any:
    """
        Listing of users
    """
    record_query = db.query(models.User).all()

    return record_query

@router.post('/users', response_model= schemas.UserInDBBase)
def create_a_user(
    obj_in: schemas.UserCreate,
    db: Session = Depends(dependencies.get_db)
    #current_user: models.User = Depends(dependecies.(roles = ['Administrator', 'SuperAdmin']))
) -> Any:
    """
        Create a user
    """

    return None