#   encoding: utf-8

#   Import Modules
from typing import Any
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, Query

from app.main import crud, models, schemas
from app.main.core import dependencies

#   Variables Definitions
router = APIRouter(
    prefix= '/users',
    tags= ['users']
)

#   Functions Definitions
@router.get('/users', response_model = schemas.UserDisplayList)
def listing_of_users(
    page: int = 0,
    per_page: int = 10,
    keyword: str = '',
    order: str = 'desc',
    status: str = Query('', enum=[models.Status.ACTIVED, models.Status.UNACTIVED]),
    db: Session = Depends(dependencies.get_db)
) -> Any:
    """
        Listing of users
    """
    users = crud.user.find_users(
        db = db, 
        page = page, 
        per_page = per_page, 
        order = order, 
        keyword = keyword
        )

    return users

@router.post('/users', response_model= schemas.UserInDBBase)
def create_a_user(
    obj_in: schemas.UserCreate,
    db: Session = Depends(dependencies.get_db)
    #current_user: models.User = Depends(dependecies.(roles = ['Administrator', 'SuperAdmin']))
) -> Any:
    """
        Create a user
    """

    return None