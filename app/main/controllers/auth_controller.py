#   encoding: utf-8

#   Import Modules
from typing import Any
from datetime import timedelta
from fastapi.exceptions import HTTPException
from sqlalchemy.orm import Session
from fastapi import APIRouter, Body, Depends

from app.main.core import dependencies
from app.main import crud, models, schemas
from app.main.core.config import Config

#   Variables Definitions
router = APIRouter(
    prefix= '/auth',
    tags= ['auth']
)

#   Routes Definitions
@router.post('/users')
def signin_email_password(
    email: str = Body(...),
    password: str = Body(...),
    db: Session = Depends(dependencies.get_db)
) -> Any:
    """
        SignIn with email and password
    """
    user = crud.user.authenticate(db = db, email = email, password = password)

    if not user:
        raise HTTPException(status_code = 403, detail = 'user-logged-fail')
    elif not crud.user.is_active(user):
        raise HTTPException(status_code = 403, detail = 'user-inactive-account')