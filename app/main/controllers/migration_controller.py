#   encoding: utf-8

#   Import Modules
import os
import shutil
import logging
import platform

from typing import Any
from dataclasses import dataclass
from sqlalchemy.orm import Session
from sqlalchemy import Column, String
from fastapi import APIRouter, Body, Depends, HTTPException

from app.main import schemas
from app.main.core import dependencies
from app.main.core.config import Config
from app.main.models.db.base_class import Base

#   Configurations Defintions
logging.basicConfig(level= logging.INFO)

#   Variables Definitions
logger = logging.getLogger(__name__)
router = APIRouter(
    prefix= '/migration',
    tags= ['migration']
)

#   Functions Definitions
def check_user_access_key(admin_key: schemas.AdminKey):
    logger.info(f'Check user access token key: {admin_key}')
    if admin_key.key not in [Config.ADMIN_KEY]:
        raise HTTPException(status_code= 400, detail= 'invalid-access-key')

#   Routes Definitions
@router.post('/create-database-tables', status_code = 201)
async def create_database_tables(
    admin_key: schemas.AdminKey = Body(...),
    db: Session = Depends(dependencies.get_db)
) -> Any:
    """
        Create database structure(Tables)
    """

    check_user_access_key(admin_key)

    """ Try to remove previous alembic tags in database """
    try:
        @dataclass
        class AlembicVersion(Base):
            __tablename__ = "alembic_version"
            version_num: str = Column(String(32), primary_key=True, unique=True)
        db.query(AlembicVersion).delete()
        db.commit()
    except Exception as e:
        pass

    """ Try to remove previous alembic versions folder """
    migrations_folder =  os.path.join(os.getcwd(), "alembic/versions")
    try:
        shutil.rmtree(migrations_folder)
    except Exception as e:
        pass

    """ create alembic versions folder content """
    try:
        os.mkdir(migrations_folder)
    except OSError:
        logger.info("Creation of the directory %s failed" % migrations_folder)
    else:
        logger.info("Successfully created the directory %s " % migrations_folder)


    try:
        # Get the environment system
        if platform.system() == 'Windows':
            os.system('set PYTHONPATH=. && alembic revision --autogenerate')
            os.system('set PYTHONPATH=. && alembic upgrade head')
        else:
            os.system('PYTHONPATH=. alembic revision --autogenerate')
            os.system('PYTHONPATH=. alembic upgrade head')

        """ Try to remove previous alembic versions folder """
        migrations_folder =  os.path.join(os.getcwd(), "alembic/versions")
        try:
            shutil.rmtree(migrations_folder)
        except Exception as e:
            pass

        return {"detail": "Table de la base de données crées avec success"}
                
    except Exception as e:
        raise HTTPException(status_code = 500, detail = str(e))
