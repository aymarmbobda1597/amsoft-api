#   encoding: utf-8

#   Import Modules
from os import path
from uuid import uuid4
from os import mkdir, getcwd
from datetime import datetime
from fastapi import UploadFile

#   Variables Definitions
upload_folder = path.join(path.dirname(), 'static', 'upload')

#   Functions Definitions
async def write_file_on_server(destination: str, file: UploadFile):
    file_size= 0
    file_name= f'{destination}/' + file.filename.replace(' ', '-')
    
    with open(file_name, 'wb') as image:
        content = await file.read()
        image.write(content)
        image.close()
        file_size = len(content)

    return {
        'file_path': file_name,
        'file_size': file_size,
        'content-type': file.content_type
    }


def upload_file():
    pass

def download_file():
    pass